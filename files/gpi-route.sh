#!/bin/bash
mask2cidr() {
    nbits=0
    IFS=.
    for dec in $1 ; do
        case $dec in
            255) let nbits+=8;;
            254) let nbits+=7;;
            252) let nbits+=6;;
            248) let nbits+=5;;
            240) let nbits+=4;;
            224) let nbits+=3;;
            192) let nbits+=2;;
            128) let nbits+=1;;
            0);;
            *) echo "Error: $dec is not recognised"; exit 1
        esac
    done
    echo "$nbits"
}
CLIENT_CONFIG=/etc/openvpn/server/gpi-clients/$common_name
if [ -f $CLIENT_CONFIG ]; then
	ROUTE=`cat $CLIENT_CONFIG | grep iroute | sed 's/iroute \(.*\)/\1/'`
	NET=`echo $ROUTE | cut -d ' ' -f1`
	NETMASK=`echo $ROUTE | cut -d ' ' -f2`
        NUMBITS=$(mask2cidr $NETMASK)
	if [ "$script_type" == "client-connect" ]; then
		ip route add $NET/$NUMBITS via $ifconfig_pool_remote_ip
	elif [ "$script_type" == "client-disconnect" ]; then
		ip route del $NET/$NUMBITS via $ifconfig_pool_remote_ip
	else
		echo "unknown: $script_type"
	fi
fi

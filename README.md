# OpenVPN für GPI
## Anlegen neuer Clients
Der OpenVPN-Server nutzt Clientzertifikate zur Authentifizierung der Clients.
Dazu haben wir mit [easy-rsa]  eine eigene kleine PKI (Public-Key-Infrastructure) auf Ihrer VPN-VM unter /etc/ssl/easy-rsa/ aufgesetzt.

Im Ordner 'keys/' befinden sich die erzeugten Zertifikate (`*.crt`), die privaten Schlüssel (`*.key`) und die erzeugen Zertifikatsanträge (`*.csr`, für Sie unwichtig, diese werden während der Erzeugung der Zertifikate angelegt). In diesem Ordner befindet sich auch das Zertifikat der CA (`ca.crt`), sowie deren Privater Schlüssel (`ca.key`). Diesen Key am besten nie Anfassen oder kopieren. Verlässt dieser den Server ist davon auszugehen, dass er kompromitiert ist und alle Zertifikate auf den Clients ausgetauscht werden müssen.

Um ein neues Zertifikat mit privatem Schlüssel für einen neuen Client zu erzeugen, sind folgende Schritte nötig:

1. Wechsel in den easy-rsa-Ordner:
`cd /etc/ssl/easy-rsa/`
2. Konfiguration laden
`source ./vars`
3. Zertifikat anlegen:
`./build-key <name des hinzuzufügenden Clients>`

In dem folgenden Prozess werden einige Daten abgefragt. Der Prozess ist aber so gestaltet, dass immer der vorgeschlagene Wert übernommen werden kann. Abschließend kommen zwei yes/no-Abfragen, die beide mit einem '`yes`' zu bestätigen sind.
Daraufhin befinden sich im '`keys/`'-Ordner das signierte Zertifikat und der Private-Key (<name des hinzuzufügenden Clients>.crt/.key).

Zur Einrichtung der Clients benötigen Sie auf den Clients dann das ca.crt und das crt/key-File aus dem obigen Prozess.

Eine Beispielkonfiguration liegt auf dem RUT995 (Services -> VPN) und dem Insys-Router (Dial-Out -> OpenVPN Client) vor.

Die Externe IP für die VM ist `129.13.70.118`.

Der OpenVPN-Server verteilt an die Clients Adressen aus dem Netz `129.13.74.64/26`.
Die verteilten Adressen können in `/etc/openvpn/server/ipp.txt` eingesehen werden. 

Soll an einen Client ein Subnetz deligiert werden, muss im Ordner `/etc/openvpn/server/gpi-clients/` eine Datei mit dem namen des hinzugefügten Clients erzeugt und mit folgendem Inhalt gefüllt werden:
```
iroute <netzadresse des zu delegierenden Netzes, z.B. 192.168.0.0> <netzmaske des zu delegierenden Netzes, z.B. 255.255.255.0>
```

## Beispielconfig
```
client
remote 129.13.70.118
port 1194
dev tun
proto tcp
nobind
tls-version-min 1.2
ca ca.crt
key client.key
cert client.crt
cipher AES-256-CBC
verb 3
```

[easy-rsa]: https://openvpn.net/index.php/open-source/documentation/miscellaneous/77-rsa-key-management.html
